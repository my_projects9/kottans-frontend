###### kottans-frontend

# Git and GitHub

1. Finish the course [Version Control with Git](https://www.udacity.com/course/version-control-with-git--ud123) ✅

![Result](/img/Version%20Control%20Git.png)

2. Complete the following levels at [learngitbranching.js.org](https://learngitbranching.js.org/): ✅
* Main: Introduction Sequence
* Remote: Push & Pull -- Git Remotes

![Result_Main](/img/Main.png)
![Result_Remote](/img/Remote.png)

# Linux CLI, and HTTP

1. [Linux Survival (4 modules)](https://linuxsurvival.com/linux-tutorial-introduction/) ✅

![Quiz1](/task_linux_cli/Quiz1.png)
![Quiz2](/task_linux_cli/Quiz2.png)
![Quiz3](/task_linux_cli/Quiz3.png)
![Quiz4](/task_linux_cli/Quiz4.png)

2. [HTTP: The Protocol Every Web Developer Must Know - Part 1](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177) ✅

3. [HTTP: The Protocol Every Web Developer Must Know - Part 2](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-2--net-31155) ✅

# Git Collaboration

1. [GitHub & Collaboration](https://classroom.udacity.com/courses/ud456) ✅

![GitHub&Collaboration](/task_git_collaboration/GitHub&Collaboration.png)

2. Complete the following levels at [learngitbranching.js.org](https://learngitbranching.js.org/): ✅
* Main: Ramping Up, Moving Work Around
* Remote: To Origin and Beyond

![main](/task_git_collaboration/main.png)
![remote](/task_git_collaboration/remote.png)